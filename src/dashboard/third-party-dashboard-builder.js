/**
 * Created by jlong on 1/4/17.
 */
import _ from 'lodash'
import 'ui-router-extras-core';
import 'ui-router-extras-future';
import DashboardTemplate from './dashboard.tpl.html!text';
import DashboardController from './dashboard.controller';

import demoTiles from '../demo/third-party.json!';

var dashboardBuilder = function (module) {
    module.requires = _.union(module.requires, ['ct.ui.router.extras.future']);
    var RouterConfig = ['$futureStateProvider', function ($futureStateProvider) {
        $futureStateProvider.stateFactory('tile', ['$q', 'futureState', function ($q, futureState) {
            var state = {
                name: "dashboard",
                url: '/dashboard',
                views: {
                    "": {
                        template: DashboardTemplate,
                        controller: DashboardController
                    },
                }
            };

            var tileKeys = _.keys(demoTiles);
            var tilePromises = [];
            tileKeys.forEach(function (key) {
                tilePromises.push(System.import(demoTiles[key]).then(builder => {
                    state.views[key] = builder.BuildViews();
                }))
            });

            return $q.all(tilePromises).then(function () {
                return state;
            });
        }]);

        $futureStateProvider.futureState({
            stateName: 'dashboard',
            url: '/dashboard',
            type: 'tile',
        });
    }];

    return RouterConfig;
};

export default dashboardBuilder;