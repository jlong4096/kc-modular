/**
 * Created by jlong on 12/22/16.
 */
import _ from 'lodash/core'
import tiles from '../demo/third-party.json!';

class AppController {
    constructor ($scope, $state) {
        $scope.title = 'Test';
        $scope.install = function () {
            $scope.show = true;
        };

        $scope.tiles = _.keys(tiles);
    }
}
AppController.$inject = ['$scope', '$state'];
export default AppController;
