/**
 * Created by jlong on 1/3/17.
 */
import _ from 'lodash'
import angular from 'angular';
import 'oclazyload';
import 'ui-router-extras-core';
import 'ui-router-extras-future';
import thirdPartyRoutes from './third-party-routes.json!';

var thirdPartyRouting = function (module) {
    module.requires = _.union(module.requires, ['ct.ui.router.extras.future', 'oc.lazyLoad'] )
    var RouterConfig = ['$futureStateProvider', function ($futureStateProvider) {
        $futureStateProvider.stateFactory('load', ['$q', '$ocLazyLoad', 'futureState', function($q, $ocLazyLoad, futureState) {
            var def = $q.defer();
            System.import(futureState.src).then(loaded => {
                var newModule = loaded;
                if (!loaded.name && angular.isDefined(loaded) && angular.isDefined(loaded.default)) {
                    newModule = loaded.default;
                }

                $ocLazyLoad.load(newModule).then(function() {
                    def.resolve();
                }, function() {
                    console.log('error loading: ' + newModule.name);
                    def.reject();
                });
            });

            return def.promise;
        }]);

        thirdPartyRoutes.forEach(function(r) {
            $futureStateProvider.futureState(r);
        });
    }];

    return RouterConfig;
};

export default thirdPartyRouting;