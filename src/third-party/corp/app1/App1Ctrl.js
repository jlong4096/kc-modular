/**
 * Created by jlong on 1/3/17.
 */

class App1Controller {
    constructor ($scope) {
        this.$scope = $scope;
        this.$scope.title = "Success!";
    }
}

App1Controller.$inject = ['$scope'];

export default App1Controller;