/**
 * Created by jlong on 1/3/17.
 */

import angular from 'angular';
import App1Controller from './App1Ctrl';
import App1Template from './app1.tpl.html!text';

function ConfigureModule($stateProvider) {
    $stateProvider.state('app1', {
        url: '/app1',
        views: {
            "": {
                //templateUrl: 'third-party/corp/app1/app1.tpl.html',
                template: App1Template,
                controller: App1Controller
            }
        }
    });
};

export default angular.module('App1', [/*'third-party/corp/app1/app1.tpl.html'*/]).config(['$stateProvider', ConfigureModule]);