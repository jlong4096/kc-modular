/**
 * Created by jlong on 1/5/17.
 */
import App1Controller from './App1Ctrl';
import App1Template from './app1.tpl.html!text';

export function BuildViews() {
    return {
        template: App1Template,
        controller: App1Controller
    }
}