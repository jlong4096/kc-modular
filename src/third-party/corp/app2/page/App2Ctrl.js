/**
 * Created by jlong on 1/5/17.
 */
class App2Controller {
    constructor ($scope) {
        this.$scope = $scope;
        this.$scope.title = "Page";
    }
}

App2Controller.$inject = ['$scope'];

export default App2Controller;