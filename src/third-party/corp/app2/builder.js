/**
 * Created by jlong on 1/5/17.
 */
import App2TileController from './tile/App2Ctrl';
import App2TileTemplate from './tile/app2.tpl.html!text';

export function BuildViews() {
    return {
        template: App2TileTemplate,
        controller: App2TileController
    }
}