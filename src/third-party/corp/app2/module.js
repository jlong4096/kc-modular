/**
 * Created by jlong on 1/5/17.
 */
import angular from 'angular';
import App2PageController from './page/App2Ctrl';
import App2PageTemplate from './page/app2.tpl.html!text';

function ConfigureModule($stateProvider) {
    $stateProvider.state('app2', {
        url: '/app2',
        views: {
            "": {
                template: App2PageTemplate,
                controller: App2PageController
            }
        }
    });
};

export default angular.module('App2', []).config(['$stateProvider', ConfigureModule]);