/**
 * Created by jlong on 1/5/17.
 */
import angular from 'angular';
import 'angular-ui-router';
import dashboardBuilder from './dashboard/third-party-dashboard-builder';
import thirdPartyRouting from './third-party-routing';

let app = angular.module('KukaConnect', ['ui.router']);
app.config(dashboardBuilder(app));
app.config(thirdPartyRouting(app));

app.config(['$urlRouterProvider', '$stateProvider', '$httpProvider', '$logProvider', '$compileProvider', function ($urlRouterProvider, $stateProvider, $httpProvider, $logProvider, $compileProvider) {
    $urlRouterProvider.otherwise('/dashboard');

    // $locationProvider.html5Mode({
    //     enabled: true,
    //     requireBase: false
    // });

    $httpProvider.useApplyAsync(true);

    if(window.prod){
        $logProvider.debugEnabled(false);
        // http://ng-perf.com/2014/10/24/simple-trick-to-speed-up-your-angularjs-app-load-time/
        $compileProvider.debugInfoEnabled(false);
    }
}]);

angular.element(document).ready(function() {
    angular.bootstrap(document.body, [app.name], {
        strictDi: true
    });
});

export default app;