KUKA Connect Modular Demo
=========================

## Overview

Demo project for SystemJS with ES6 syntax using Babel and can lazy-load AngularJS modules.

#### This project does:
- SystemJS/JSPM
- ES6
- Transpiles with Babel
- AngularJS (1.6)
- Lazy-loads Angular modules via routes and ocLazyLoad
- UI Router with Future States
- Gulp

#### This project does not:
- Watch (for changes)
- Host (assumes hosted via IntelliJ)
- CSS/LESS
- Fully understands modules
- Unit/E2E tests
- Minification
- Tree shaking?

### Install

0.  `npm install jspm -g` -- This is only necessary for the first installation.  May require elevated privileges.
1.  `npm install` -- This includes a post-installation step to install JSPM modules.

### Build

0.  `gulp build`
1.  Navigate to:  http://localhost:63342/kc-modular/build/app/#!/dashboard (Assumes IntelliJ is running)

### Adding packages
#### Production
Always use JSPM.  Examples:
```
$ jspm install npm:lodash-node
$ jspm install github:components/jquery
$ jspm install jquery
$ jspm install myname=npm:underscore
```

This will automatically update npm's package.json.

#### Devlopment (ie, build tools)
Always use NPM with the --save-dev option.  Example:
```
$ npm install --save-dev gulp-changed
```

## Credits and Inspiration

- [Martin Micunda](https://github.com/martinmicunda/employee-scheduling-ui)
- [Swimlane](https://github.com/swimlane/angular1-systemjs-seed)
- [SystemJS/JSPM](http://jspm.io/docs/getting-started.html)
- [Future States](https://christopherthielen.github.io/ui-router-extras/#/future)
- [ocLazyLoad](https://github.com/ocombe/ocLazyLoad)