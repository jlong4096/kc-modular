/**
 * Created by jlong on 12/22/16.
 */
module.exports = {
    source: 'src/**/*.js',
    index: 'src/**/index.html',
    //json: 'src/**/*.json',
    templates: 'src/**/*.tpl.html',
    less: ['src/**/*.less'],
    output: 'build/app/',
    outputCss: 'build/app/**/*.css',
    landing: 'build/',
    //tests: 'test/e2e/**/*.spec.js',
    json: 'src/**/*.json'
};