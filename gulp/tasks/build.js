/**
 * Created by jlong on 12/22/16.
 */
var gulp = require('gulp');
var changed = require('gulp-changed');
var plumber = require('gulp-plumber');
//var babel = require('gulp-babel');
var sourcemaps = require('gulp-sourcemaps');
var ngAnnotate = require('gulp-ng-annotate');
var less = require('gulp-less');
//var lessPluginCleanCSS = require("less-plugin-clean-css");
var htmlmin = require('gulp-htmlmin');
var ngHtml2Js = require("gulp-ng-html2js");
var runSequence = require('run-sequence');
//var browserSync = require('browser-sync');

var concat = require('gulp-concat');
var uglify = require('gulp-uglify');

var paths = require('../paths');
//var compilerOptions = require('../babelOptions');

// var cleancss = new lessPluginCleanCSS({
//     advanced: true,
//     keepSpecialComments: 0,
//     keepBreaks: false
// });

gulp.task('build', function (callback) {
    return runSequence( 'clean', ['assets', 'less', 'tpl', 'es6', 'move'], callback);
});

gulp.task('es6', function () {
    return gulp.src(paths.source, { base: 'src' })
        .pipe(plumber())
        .pipe(changed(paths.output, { extension: '.js' }))
        //.pipe(sourcemaps.init({loadMaps: true}))
        //.pipe(babel(compilerOptions))
        //.pipe(ngAnnotate({sourceMap: true, gulpWarnings: false}))
        //.pipe(sourcemaps.write("/sourcemaps", { sourceRoot: '/src' }))
        .pipe(gulp.dest(paths.output))
});

gulp.task('tpl', function () {
    return gulp.src(paths.templates)
        .pipe(plumber())
        .pipe(changed(paths.output, {extension: '.tpl.html'}))
        .pipe(htmlmin())
        // .pipe(ngHtml2Js({
        //     //moduleName: "Partials",
        //     //prefix: "/partials"
        // }))
        // .pipe(concat("partials.min.js"))
        // .pipe(uglify())
        .pipe(gulp.dest(paths.output))
});

gulp.task('less', function () {
    return gulp.src(paths.less)
        .pipe(plumber())
        //.pipe(changed(paths.output, {extension: '.css'}))
        .pipe(sourcemaps.init())
        .pipe(less())
        .pipe(sourcemaps.write("."))
        .pipe(gulp.dest(paths.output))
        //.pipe(browserSync.reload({ stream: true }));
});

gulp.task('assets', function () {
    return gulp.src([paths.json, paths.index])
        .pipe(plumber())
        .pipe(gulp.dest(paths.output))
});