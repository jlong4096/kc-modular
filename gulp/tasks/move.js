/**
 * Created by jlong on 12/22/16.
 */
var gulp = require('gulp');
var paths = require('../paths');

gulp.task('move', function () {
    var srcPaths = [
        // './src/**/*.svg',
        // './src/**/*.woff',
        // './src/**/*.ttf',
        // './src/**/*.png',
        // './src/**/*.ico',
        // './src/**/*.jpg',
        // './src/**/*.gif',
        // './src/**/*.eot',
        './jspm_packages/**/*',
        './config.js',
    ];

    return gulp.src(srcPaths, {base: '.'})
        .pipe(gulp.dest(paths.output))
});