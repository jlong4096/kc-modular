/**
 * Created by jlong on 12/22/16.
 */
var gulp = require('gulp');
var paths = require('../paths');
var del = require('del');
var vinylPaths = require('vinyl-paths');

gulp.task('clean', function () {
    return gulp.src([paths.output, paths.landing])
        .pipe(vinylPaths(del));
});
